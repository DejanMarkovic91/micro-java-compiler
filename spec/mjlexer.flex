package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;

%%

%{

	StringBuilder string=new StringBuilder();

	//ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type){
		return new Symbol(type,yyline+1,yycolumn);
	}
	
	private Symbol new_symbol(int type, Object value){
		return new Symbol(type,yyline+1,yycolumn,value);
	}

%}

constChar = "'"([\x00-\x26]|[\x27-\x7f])"'"

%cup
%line
%column

%xstate COMMENT
%xstate STRING

%eofval{
	return new_symbol(sym.EOF);
%eofval}

%%

" "		{ }
"\b"	{ }
"\t"	{ }
"\r\n"	{ }
"\f"	{ }

"program" 	{ return new_symbol(sym.PROG, yytext()); }
"print" 	{ return new_symbol(sym.PRINT, yytext()); }
"read"		{ return new_symbol(sym.READ, yytext()); }
"return" 	{ return new_symbol(sym.RETURN, yytext()); }
"const" 	{ return new_symbol(sym.CONST, yytext()); }
"class" 	{ return new_symbol(sym.CLASS, yytext()); }
"extends" 	{ return new_symbol(sym.EXTENDS, yytext()); }
"if" 		{ return new_symbol(sym.IF, yytext()); }
"else"	 	{ return new_symbol(sym.ELSE, yytext()); }
"while" 	{ return new_symbol(sym.WHILE, yytext()); }
"break" 	{ return new_symbol(sym.BREAK, yytext()); }
"new" 		{ return new_symbol(sym.NEW, yytext()); }
"void" 		{ return new_symbol(sym.VOID, yytext()); }
"this"		{ return new_symbol(sym.THIS, yytext()); }
"++" 		{ return new_symbol(sym.INC, yytext()); }
"--" 		{ return new_symbol(sym.DEC, yytext()); }
"+" 		{ return new_symbol(sym.PLUS, yytext()); }
"-" 		{ return new_symbol(sym.MINUS, yytext()); }
"*" 		{ return new_symbol(sym.MUL, yytext()); }
"/" 		{ return new_symbol(sym.DIV, yytext()); }
"%" 		{ return new_symbol(sym.REM, yytext()); }
"=" 		{ return new_symbol(sym.EQUAL, yytext()); }
"."			{ return new_symbol(sym.POINT, yytext()); }
";" 		{ return new_symbol(sym.SEMI, yytext()); }
"," 		{ return new_symbol(sym.COMMA, yytext()); }
"(" 		{ return new_symbol(sym.LPAREN, yytext()); }
")" 		{ return new_symbol(sym.RPAREN, yytext()); }
"[" 		{ return new_symbol(sym.SQLPAREN, yytext()); }
"]" 		{ return new_symbol(sym.SQRPAREN, yytext()); }
"{" 		{ return new_symbol(sym.LBRACE, yytext()); }
"}"			{ return new_symbol(sym.RBRACE, yytext()); }
"&&"		{ return new_symbol(sym.ANDAND, yytext()); }
"||"		{ return new_symbol(sym.OROR, yytext()); }
"!="		{ return new_symbol(sym.NOTEQ, yytext()); }
">"			{ return new_symbol(sym.GREATER, yytext()); }
">="		{ return new_symbol(sym.GROREQU, yytext()); }
"<"			{ return new_symbol(sym.LOWER, yytext()); }
"<="		{ return new_symbol(sym.LOWOREQU, yytext()); }
"=="		{ return new_symbol(sym.SAME, yytext()); }

"//" 				{ yybegin(COMMENT); }
<COMMENT> . 		{ yybegin(COMMENT); }
<COMMENT> "\r\n" 	{ yybegin(YYINITIAL); }
"/*" [^*] ~"*/" | "/*" "*"+ "/"		{ }

//([0-9]+)([/x41-/xdc]|[/x61-/x7a])+		{ System.err.println("Identifikator ne sme poceti brojem greska u liniji "+(yyline+1) + " i koloni "+(yycolumn+1) ); yybegin(YYINITIAL); }
"true"|"false"								{ return new_symbol(sym.BOOLCONST,Boolean.parseBoolean(yytext())); }
[0-9]+										{ return new_symbol(sym.NUMBER, new Integer (yytext())); }
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* 				{ return new_symbol (sym.IDENT, yytext()); }
{constChar}									{ return new_symbol(sym.CHARCONST,yytext().charAt(1)); }
"'"(\\')"'"									{ return new_symbol(sym.CHARCONST,'\''); }
"'"(\\n)"'"									{ return new_symbol(sym.CHARCONST,'\n'); }
"'"(\\t)"'"									{ return new_symbol(sym.CHARCONST,'\t'); }

<YYINITIAL> \"          		 { string.setLength(0); yybegin(STRING); }
<STRING> {
  \\t                            { string.append('\t'); }
  \\n                            { string.append('\n'); }
  \\\"                           { string.append("\""); }
  \\\'                           { string.append('\''); }
  \"                             { yybegin(YYINITIAL); return new_symbol(sym.STRCONST, string.toString()); }
  \\                             { System.err.println("Nevalidan karakter \\ u stringu "+(yyline+1) + " i koloni "+(yycolumn+1) );
  								   yybegin(YYINITIAL);  }
  \r\n						 	 { System.err.println("Nezatvoren string u liniji "+(yyline+1) + " i koloni "+(yycolumn+1) );
  								   yybegin(YYINITIAL); 
  								 }
  \\\\						 	 { string.append('\\'); }
  <<EOF>>						 {
  									System.err.println("Nezatvoren string u liniji "+(yyline+1) + " i koloni "+(yycolumn+1) );
  									return new_symbol(sym.EOF);
  								 }
  .                 { string.append( yytext() ); }
}

. { System.err.println("Leksicka greska ("+yytext()+") u liniji "+(yyline+1) + " i koloni ("+(yycolumn+1) +")" ); }