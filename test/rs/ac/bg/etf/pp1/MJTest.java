package rs.ac.bg.etf.pp1;

import java.io.*;

import java_cup.runtime.Symbol;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import rs.ac.bg.etf.pp1.util.Log4JUtils;

public class MJTest {

	static {
		DOMConfigurator.configure(Log4JUtils.instance().findLoggerConfigFile());
		Log4JUtils.instance().prepareLogFile(Logger.getRootLogger());
	}

	public static void main(String[] args) throws IOException {
		Logger log = Logger.getLogger(MJTest.class);
		File sourceCode = null;
		Reader br = null;
		if (args.length != 0) {
			sourceCode = new File(args[0]);
			String filename = sourceCode.getName();
			String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
			if(!extension.equals("mj")){
				System.err.println("Input file must have proper extension. (.mj)");
				return;
			}
		} else {
			System.err.println("No input file!");
			return;
		}
		try {
			log.info("Compiling source file: " + sourceCode.getAbsolutePath());
			br = new BufferedReader(new FileReader(sourceCode));

			Yylex lexer = new Yylex(br);
			Symbol currToken = null;
			while ((currToken = lexer.next_token()).sym != sym.EOF) {
				if (currToken != null) {
					log.info(currToken.toString() + " "
							+ currToken.value.toString());
				}
			}
		}catch(Exception e){
			log.error(e.getMessage(),e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e1) {
					log.error(e1.getMessage(),e1);
				}
		}
	}
}
