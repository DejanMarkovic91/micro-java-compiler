Micro Java compiler
=============

This is simplified Java (Micro Java) compiler written in Java. (Yep, the irony)  

Specification of Micro Java language, tutorials and tool setting up is shown here:

	http://ir4pp1.etf.rs/Domaci.html
